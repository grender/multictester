import java.net.{InetAddress, InetSocketAddress}
import java.nio.file.{StandardOpenOption, Paths, Files}
import java.text.SimpleDateFormat
import java.util.{Date, UUID}

import akka.actor._
import akka.io.Tcp._
import akka.io.{IO, Tcp}
import akka.util.ByteString
import com.typesafe.config.Config

import scala.io.StdIn


object SuperLoger extends App {
  var system = ActorSystem("superLogerSystem")
  var serverActor = system.actorOf(Props[SuperLogerServer], "server")

  var command = StdIn.readLine()
  while (command != "quit") {
    command.toLowerCase match {
      case "run" =>
      case _ =>
    }
    command = StdIn.readLine()
  }
  system.shutdown
}

object SuperLogerHandler {
  sealed trait State

  sealed trait Data

  object State {

    case object Created extends State
    case object Registered extends State
  }

  object Data {
    val dateFormater=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss:SS")

    case class LogName(startDate:Date,logName:String) extends Data {
      def fullName="%s %s.log".format(dateFormater.format(startDate),logName)
    }
  }


  object Command {
    case class WriteToFile(data:ByteString)
  }

  object Response {
  }

}

class SuperLogerHandler(tcp: ActorRef) extends FSM[SuperLogerHandler.State, SuperLogerHandler.Data] with ActorLogging {
  import SuperLogerHandler._

  def pathForName(logName:String)=Paths.get("/Volumes","Temp","cmsgLogs",logName)

  def writeToLog(logName:String,data:ByteString) {
    val out=Files.newByteChannel(pathForName(logName),StandardOpenOption.CREATE,StandardOpenOption.APPEND,StandardOpenOption.WRITE)
    out.write(data.toByteBuffer)
    out.close
  }

  startWith(State.Created, Data.LogName(new Date,UUID.randomUUID.toString))

  when(State.Created) {
    case Event(Tcp.Received(data), oldLogName@Data.LogName(_,_)) => {
      writeToLog(oldLogName.fullName,data)
      val RegisterCmdR="""^.*MagickSip.c .*:(.*) .*$""".r
      data.utf8String match {
        case RegisterCmdR(login)=> {
          log.info("Client acc found: %s",login)
          var newLogName=Data.LogName(oldLogName.startDate,login.trim)
          Files.move(pathForName(oldLogName.fullName),pathForName(newLogName.fullName))
          goto(State.Registered) using newLogName
        }
        case _=> stay
      }
    }
  }

  when(State.Registered) {
    case Event(Tcp.Received(data),logName@Data.LogName(_,_))=> {
      writeToLog(logName.fullName,data)
      stay
    }
  }
  whenUnhandled {
    case Event(PeerClosed, s) => {
      log.info("Client disconnected")
      context.stop(self)
      stay
    }

    case Event(e, s) => {
      log.info("We not waiting this message:{} State:{}", e, s)
      stay
    }
  }
}

class SuperLogerServer(config:Config) extends Actor with ActorLogging {

  import Tcp._
  import context.system

  IO(Tcp) ! Bind(self, new InetSocketAddress(InetAddress.getByName(config.getString("address")), 60012))

  def receive = {
    case b@Bound(localAddress) =>
      log.info("Bounded to address...")

    case CommandFailed(_: Bind) => context stop self

    case c@Connected(remote, local) =>
      log.info("Client connected...")
      val connection = sender()
      val handler = context.actorOf(Props(classOf[SuperLogerHandler], connection))
      connection ! Register(handler)

  }
}