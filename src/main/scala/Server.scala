import akka.actor.FSM.Event
import akka.actor._
import akka.event.slf4j.Logger
import akka.io.Tcp.Write
import akka.io.{IO, Tcp}
import akka.util.ByteString
import java.net.{InetAddress, InetSocketAddress}
import akka.io.{IO, Tcp}
import com.typesafe.config.Config


import scala.collection.mutable.Map

object ClientsInfo {
  val clients = Map.empty[String, ActorRef]
}

object MagicTransport extends Enumeration {
  type MagicTransport = Value
  val UDP, TCP, TLS = Value
}

abstract trait Cmd

object ServerCmd extends Enumeration with Cmd {
  type ServerCmd = Value
  val Registered,
      Init,
      Connect,
      Call,
      Destroy,
      AnswerCall,
      TerminateAllCalls,
      Reregister,
      VoiceChange,
      Unknown = Value

  private val stringMap = values.groupBy(_.toString.toLowerCase).map { case (k, v) => (k, v.head)}

  def fromString(s: String) = stringMap.get(s.toLowerCase) match {
    case Some(v) => v
    case None => Unknown
  }

  def unapply(s:String):Option[ServerCmd] = Some(fromString(s))
}

object ClientCmd extends Enumeration with Cmd {
  type ClientCmd = Value
  val Register,
      Inited,
      Connected,
      IncomingCall,
      CallAnsweredAndZrtpSasShowed,
      CallStateChanged,
      Destroed,
      Unknown = Value

  private val stringMap = values.groupBy(_.toString.toLowerCase).map { case (k, v) => (k, v.head)}

  def fromString(s: String) = stringMap.get(s.toLowerCase) match {
    case Some(v) => v
    case None => Unknown
  }

  def unapply(s:String):Option[ClientCmd]=Some(fromString(s))
}

object CodecMode extends Enumeration {
  type CodecMode = Value
  val Speex8000,
  Codec2,
  Cryptobook = Value
}

object MulticClient {

  import MagicTransport._

  sealed trait State

  sealed trait Data

  object State {

    case object Created extends State

    case object Registered extends State

    case object Inited extends State

    case object Connected extends State


  }

  object Data {

    case object Empty extends Data
    case class Platform(platform:String) extends Data
    case class RequestSender(platform:String,sourceSender: ActorRef) extends Data

  }


  object Command {

    case class Init(transport: MagicTransport,codec2only:Boolean)

    case class Connect(user: String, password: String, server: String)

    case class Call(sipUrl:String,codecMode:CodecMode.CodecMode)

    case object Destroy

    case object TerminateAllCalls

    case class RegisterSenderActor(ref:ActorRef)

    case object AnswerCall

    case object Reregister

    case object ActivateCodec2Only

    case object ActivateCodec2CryptobookOnly

    case class ActivateVoiceChanger(mode:String)
  }

  object Response {
    case object Inited
    case object Destroed
    case object Connected
    case object IncomingCall
    case class CallAnsweredAndZrtpSasShowed(sas:String)
    case class CallStateChanged(state:String,status:String)
  }

}

case class TcpSend(txt: String)

object TcpCommand {
  val cmdRx = "<(.*)>".r

  val log=Logger("TcpCommand")

  def apply(cmd: Enumeration#Value): TcpSend = apply(cmd, "")

  def apply(cmd: Enumeration#Value, arg: String) = {
    TcpSend(cmd.toString + " " + arg)
  }

  def unapply(x: Tcp.Received): Option[(ClientCmd.ClientCmd, String)] = {
    x match {
      case Tcp.Received(data) => {
        val str = data.utf8String
        var fullCmd = str.replace("\n", "").replace("\r", "").trim.toLowerCase
        fullCmd match {
          case cmdRx(ss) => {
            var s = ss.trim
            val spcIdx = s.indexOf(" ")
            if (spcIdx > 0) {
              val cmd = s.substring(0, spcIdx).trim
              val arg = s.substring(spcIdx).trim
              Some(ClientCmd.fromString(cmd), arg)
            } else {
              Some(ClientCmd.fromString(s), "")
            }
          }
          case _ => {
            log.info("Unknown command:{}",fullCmd)
            Some(ClientCmd.Unknown, "")
          }
        }
      }
      case _ => None
    }
  }
}

class MulticClient(tcp: ActorRef) extends FSM[MulticClient.State, MulticClient.Data] {

  import context.system
  import Tcp._
  import MulticClient._


  startWith(State.Created, Data.Empty)

  when(State.Created) {
    case Event(TcpCommand(ClientCmd.Register, platform), _) => {
      self ! TcpCommand(ServerCmd.Registered)
      ClientsInfo.clients += ((platform, self))
      goto(State.Registered) using Data.Platform(platform)
    }
  }

  when(State.Registered) {
    case Event(Command.Init(transport,isCodec2), _) => {
      var codec2Arg=if(isCodec2) "codec2" else ""
      self ! TcpCommand(ServerCmd.Init, transport.toString.toLowerCase+" "+codec2Arg.toLowerCase)
      stay
    }
    case Event(TcpCommand(ClientCmd.Inited, arg), _) => goto(State.Inited)
  }
  when(State.Inited) {
    case Event(Command.Connect(user, pwd, server), _ ) => {
      self ! TcpCommand(ServerCmd.Connect, user + " " + pwd + " " + server)
      stay
    }
    case Event(TcpCommand(ClientCmd.Connected, arg), _) => goto(State.Connected)
  }

  when(State.Connected) {
    case Event(Command.Call(sipUrl,mode),_) => {
      val cryptoArg=mode.toString
      self ! TcpCommand(ServerCmd.Call, sipUrl+" "+cryptoArg)
      stay
    }

    case Event(Command.Destroy,_) => {
      self ! TcpCommand(ServerCmd.Destroy)
      stay
    }

    case Event(Command.TerminateAllCalls,_) => {
      self ! TcpCommand(ServerCmd.TerminateAllCalls)
      stay
    }

    case Event(Command.AnswerCall,_) => {
      self ! TcpCommand(ServerCmd.AnswerCall)
      stay
    }

    case Event(Command.ActivateVoiceChanger(mode),_) => {
      self ! TcpCommand(ServerCmd.VoiceChange,mode)
      stay
    }

    case Event(Command.Reregister,_) => {
      self ! TcpCommand(ServerCmd.Reregister)
      stay
    }

    case Event(TcpCommand(ClientCmd.CallAnsweredAndZrtpSasShowed,arg),Data.RequestSender(plt,ssender)) => {
      ssender ! Response.CallAnsweredAndZrtpSasShowed(arg)
      stay
    }

    case Event(TcpCommand(ClientCmd.CallStateChanged,arg),Data.RequestSender(plt,ssender)) => {
      var a=arg.trim.split(" ")
      var (state,status)=arg.trim.split(" ").toList match {
        case List(a) => (a,"")
        case List(a,b) => (a,b)
        case _ => (arg.trim,"")
      }
      ssender ! Response.CallStateChanged(state,status)
      stay
    }

    case Event(TcpCommand(ClientCmd.IncomingCall,arg),Data.RequestSender(plt,ssender)) => {
      ssender ! Response.IncomingCall
      stay
    }

    case Event(TcpCommand(ClientCmd.Destroed,arg),Data.RequestSender(plt,ssender)) => {
      ssender ! Response.Destroed
      goto(State.Registered)
    }
  }

  onTransition {
    case a => {
      val plt=stateData match {
        case Data.Platform(p)=>p
        case Data.RequestSender(p,_)=>p
        case _=> "unknown"
      }
      (a, stateData) match {
        case (_ -> State.Connected, d) => {
          log.info("{} connected",plt)
          d match {
            case Data.RequestSender(plt,ssender)=> ssender ! Response.Connected
            case _=>
          }
        }

        case (_ -> State.Inited, d) => {
          log.info("{} inited",plt)
          d match {
            case Data.RequestSender(plt,ssender)=> ssender ! Response.Inited
            case _=>
          }
        }
        case _ =>
      }
    }
  }

  whenUnhandled {
    case Event(PeerClosed, s) => {

      val plt= s match {
          case Data.Platform(p)=>p
          case Data.RequestSender(p,_)=>p
          case _=> "unknown"
        }

      log.info("Client {} disconnected",plt)
      ClientsInfo.clients -= plt
      context.stop(self)
      stay
    }

    case Event(TcpSend(txt), s) => {
      tcp ! Write(ByteString("[" + txt + "]\n"))
      stay
    }

    case Event(Command.RegisterSenderActor(ref),Data.Platform(plt))=> {
      stay using Data.RequestSender(plt,ref)
    }

    case Event(Received(e), _) => {
      log.info("We not waiting this tcp command:{}", e.utf8String)
      stay
    }

    case Event(e, s) => {
      log.info("We not waiting this message:{} State:{}", e, s)
      stay
    }
  }

}

class Server(config:Config) extends Actor with ActorLogging {

  import Tcp._
  import context.system

//  IO(Tcp) ! Bind(self, new InetSocketAddress("localhost", 61000))
//   IO(Tcp) ! Bind(self, new InetSocketAddress(InetAddress.getByAddress(Array[Byte](127,0,0,1)), 61000))
  IO(Tcp) ! Bind(self, new InetSocketAddress(InetAddress.getByName(config.getString("address")), 61000))

  def receive = {
    case b@Bound(localAddress) =>
    // do some logging or setup ...

    case CommandFailed(_: Bind) => context stop self

    case c@Connected(remote, local) =>
      log.info("Client connected...")
      val connection = sender()
      val handler = context.actorOf(Props(classOf[MulticClient], connection))
      connection ! Register(handler)

  }
}


