


import akka.actor.{ActorRef, Props, ActorSystem}
import com.typesafe.config.ConfigFactory
import org.scalatest._
import akka.testkit.{TestFSMRef, ImplicitSender, TestKit, TestActorRef}
import scala.concurrent.duration._
import scala.concurrent.Await
import akka.pattern.ask
import scala.concurrent.duration._
import MulticClient._
import MagicTransport.MagicTransport

import scala.io.StdIn

case class ClientTestSpecParam(
                             sipUrl:String,
                             plt: String,
                             client: ActorRef)

case class InitConnectTestSpecParam(
                              transport: MagicTransport,
                              connectCmd: Command.Connect,
                              plt: String,
                              client: ActorRef
                              )

abstract class MulticTest(system: ActorSystem, initedServer: ActorRef)extends TestKit(system) with ImplicitSender
with WordSpecLike with Matchers with BeforeAndAfterAll {

}

class InitConnectTestSpec(system: ActorSystem, initedServer: ActorRef, param: InitConnectTestSpecParam) extends MulticTest(system,initedServer) {

  import MulticClient._
  import param._

  override def beforeAll = {
    param.client ! Command.RegisterSenderActor(testActor)
  }

  info(s"Init $plt($transport)")

  s"""Platform "$plt" """ must {

    "inited" in {
      client ! Command.Init(transport,true)
      expectMsg(30.seconds, Response.Inited)
    }
    "connected" in {
      client ! connectCmd
      expectMsg(30.seconds, Response.Connected)
    }
  }


}

class DestorTestSpec(system: ActorSystem, initedServer: ActorRef,plt:String, client: ActorRef)  extends MulticTest(system,initedServer) {

  import MulticClient._

  override def beforeAll = {
    client ! Command.RegisterSenderActor(testActor)
  }

  info(s"Destroy $plt")

  s"""Platform "$plt" """ must {
    "deinited" in {
      client ! Command.Destroy
      expectMsg(30.seconds, Response.Destroed)
    }
  }
}

class CallTestSpec(system: ActorSystem, initedServer: ActorRef, params: Tuple2[ClientTestSpecParam, ClientTestSpecParam]) extends MulticTest(system,initedServer) {
  import MulticClient._



  override def beforeAll = {
    params._1.client ! Command.RegisterSenderActor(testActor)
    params._2.client ! Command.RegisterSenderActor(testActor)
  }

  def hangupAllCalls(prefix:String) {
    s"$prefix All calls" must {
      s"${p1.plt} hangup" in {
        p1.client ! Command.TerminateAllCalls
      }

      s"$prefix ${p2.plt} hangup" in {
        p2.client ! Command.TerminateAllCalls
      }

      Thread.sleep(5000)
    }
  }

  def call(p1:ClientTestSpecParam,p2:ClientTestSpecParam) {
    s"Call from ${p1.plt} to ${p2.plt}" must {
      s"${p2.plt} get incoming call" in {
        p1.client ! Command.Call(p2.sipUrl,CodecMode.Speex8000)
        expectMsg(30.seconds, Response.IncomingCall)
      }

      s"${p2.plt} answer to call" in {
        p2.client ! Command.AnswerCall
      }

      s"get sas's and they must euals" in {
        val Seq(sasMsg1,sasMsg2)=expectMsgAllClassOf(30.seconds,classOf[Response.CallAnsweredAndZrtpSasShowed],classOf[Response.CallAnsweredAndZrtpSasShowed])
        assert(sasMsg1.sas==sasMsg2.sas)
      }
    }
  }

  val (p1, p2) = params


  info(s"Test caling between ${p1.plt} to ${p2.plt}")

  call(p1,p2)
  hangupAllCalls("1")
  call(p2,p1)
  hangupAllCalls("2")

}

object Main extends App {

  import MulticClient._

  val config = ConfigFactory.load().getConfig("server")

  var system = ActorSystem("multicTesterSystem")
  Props
  var serverActor = system.actorOf(Props(classOf[Server],config), "server")
  var serverLogerActor = system.actorOf(Props(classOf[SuperLogerServer],config), "serverLoger")



  val accounts = List("test101", "test102", "test103", "test104", "test105")
  val password = "kmeiih1osk101"
  val server = "178.63.209.16"


  var command = StdIn.readLine()
  while (command != "quit") {
   

    command.toLowerCase match {
      case "run" => {
        runTest
      }

      case "i" => ClientsInfo.clients.values.foreach(_ ! Command.Init(MagicTransport.TLS,true))
      case "c" => {
        val userForPlatform = ClientsInfo.clients.keys.zip(accounts).groupBy(_._1).map { case (k, v) => (k, v.head._2)}
        ClientsInfo.clients.keys.foreach(plt=> {
        ClientsInfo.clients(plt) ! Command.Connect(userForPlatform(plt),password,server)
        })
      }
      case "t" => ClientsInfo.clients.values.foreach(_ ! Command.TerminateAllCalls)
      case "1t" => ClientsInfo.clients(ClientsInfo.clients.keys.toList.reverse.head) ! Command.TerminateAllCalls
      case "2t" => ClientsInfo.clients(ClientsInfo.clients.keys.toList.head) ! Command.TerminateAllCalls

      case "1a" => ClientsInfo.clients(ClientsInfo.clients.keys.toList.head) ! Command.AnswerCall
      case "2a" => ClientsInfo.clients(ClientsInfo.clients.keys.toList.reverse.head) ! Command.AnswerCall

      case "12cb" => {
        val userForPlatform = ClientsInfo.clients.keys.zip(accounts).groupBy(_._1).map { case (k, v) => (k, v.head._2)}
        val plt1=ClientsInfo.clients.keys.toList.head
        val plt2=ClientsInfo.clients.keys.toList.reverse.head
        val surl1="sip:"+userForPlatform(plt1)+"@"+server
        val surl2="sip:"+userForPlatform(plt2)+"@"+server
        ClientsInfo.clients(plt1) ! Command.Call(surl2,CodecMode.Cryptobook)
      }

      case "21cb" => {
        val userForPlatform = ClientsInfo.clients.keys.zip(accounts).groupBy(_._1).map { case (k, v) => (k, v.head._2)}
        val plt1=ClientsInfo.clients.keys.toList.head
        val plt2=ClientsInfo.clients.keys.toList.reverse.head
        val surl1="sip:"+userForPlatform(plt1)+"@"+server
        val surl2="sip:"+userForPlatform(plt2)+"@"+server
        ClientsInfo.clients(plt2) ! Command.Call(surl1,CodecMode.Cryptobook)
      }

      case "12c" => {
        val userForPlatform = ClientsInfo.clients.keys.zip(accounts).groupBy(_._1).map { case (k, v) => (k, v.head._2)}
        val plt1=ClientsInfo.clients.keys.toList.head
        val plt2=ClientsInfo.clients.keys.toList.reverse.head
        val surl1="sip:"+userForPlatform(plt1)+"@"+server
        val surl2="sip:"+userForPlatform(plt2)+"@"+server
        ClientsInfo.clients(plt1) ! Command.Call(surl2,CodecMode.Codec2)
      }

      case "21c" => {
        val userForPlatform = ClientsInfo.clients.keys.zip(accounts).groupBy(_._1).map { case (k, v) => (k, v.head._2)}
        val plt1=ClientsInfo.clients.keys.toList.head
        val plt2=ClientsInfo.clients.keys.toList.reverse.head
        val surl1="sip:"+userForPlatform(plt1)+"@"+server
        val surl2="sip:"+userForPlatform(plt2)+"@"+server
        ClientsInfo.clients(plt2) ! Command.Call(surl1,CodecMode.Codec2)
      }

      case "12" => {
        val userForPlatform = ClientsInfo.clients.keys.zip(accounts).groupBy(_._1).map { case (k, v) => (k, v.head._2)}
        val plt1=ClientsInfo.clients.keys.toList.head
        val plt2=ClientsInfo.clients.keys.toList.reverse.head
        val surl1="sip:"+userForPlatform(plt1)+"@"+server
        val surl2="sip:"+userForPlatform(plt2)+"@"+server
        ClientsInfo.clients(plt1) ! Command.Call(surl2,CodecMode.Speex8000)
      }

      case "21" => {
        val userForPlatform = ClientsInfo.clients.keys.zip(accounts).groupBy(_._1).map { case (k, v) => (k, v.head._2)}
        val plt1=ClientsInfo.clients.keys.toList.head
        val plt2=ClientsInfo.clients.keys.toList.reverse.head
        val surl1="sip:"+userForPlatform(plt1)+"@"+server
        val surl2="sip:"+userForPlatform(plt2)+"@"+server
        ClientsInfo.clients(plt2) ! Command.Call(surl1,CodecMode.Speex8000)
      }

      case "1vn" => ClientsInfo.clients(ClientsInfo.clients.keys.toList.head) ! Command.ActivateVoiceChanger("n")
      case "1v1" => ClientsInfo.clients(ClientsInfo.clients.keys.toList.head) ! Command.ActivateVoiceChanger("1")
      case "1v2" => ClientsInfo.clients(ClientsInfo.clients.keys.toList.head) ! Command.ActivateVoiceChanger("2")
      case "1v3" => ClientsInfo.clients(ClientsInfo.clients.keys.toList.head) ! Command.ActivateVoiceChanger("3")

      case "2vn" => ClientsInfo.clients(ClientsInfo.clients.keys.toList.reverse.head) ! Command.ActivateVoiceChanger("n")
      case "2v1" => ClientsInfo.clients(ClientsInfo.clients.keys.toList.reverse.head) ! Command.ActivateVoiceChanger("1")
      case "2v2" => ClientsInfo.clients(ClientsInfo.clients.keys.toList.reverse.head) ! Command.ActivateVoiceChanger("2")
      case "2v3" => ClientsInfo.clients(ClientsInfo.clients.keys.toList.reverse.head) ! Command.ActivateVoiceChanger("3")

      case "rr"=> ClientsInfo.clients.values.foreach(_ ! Command.Reregister)

      case "ccc"=> ClientsInfo.clients.values.foreach(_ ! Command.ActivateCodec2CryptobookOnly)

      case "cc"=> ClientsInfo.clients.values.foreach(_ ! Command.ActivateCodec2Only)

      case "mo"=> ClientsInfo.clients.values.foreach(_ ! Command.Call("sip:out_89150971683"+"@"+server,CodecMode.Speex8000))
      case "moc"=> ClientsInfo.clients.values.foreach(_ ! Command.Call("sip:out_89150971683"+"@"+server,CodecMode.Codec2))

      case _ =>
    }
    command = StdIn.readLine()
  }
  system.shutdown



  def runTest {
    val userForPlatform = ClientsInfo.clients.keys.zip(accounts).groupBy(_._1).map { case (k, v) => (k, v.head._2)}


    val platformCombination = ClientsInfo.clients.keys.toList.combinations(2).toList

    val transportCombination = {
      MagicTransport.values.map(t => List(t, t)).toList ::: MagicTransport.values.toList.combinations(2).toList
    }

    var i = 0

    def initPlatform(plt:String): Unit = {
      val c=ClientsInfo.clients(plt)
      val transport=MagicTransport.TLS
      val connectCmd=Command.Connect(userForPlatform(plt), password, server)
      val param=new InitConnectTestSpecParam(transport,connectCmd,plt,c)
      new InitConnectTestSpec(system,serverActor,param).execute
    }

    def destroyPlatform(plt:String): Unit = {
      val c=ClientsInfo.clients(plt)
      new DestorTestSpec(system,serverActor,plt,c).execute
    }

    ClientsInfo.clients.keys.foreach(initPlatform)

    platformCombination.foreach {
      case List(plt1, plt2) => {
        val p1 = ClientTestSpecParam("sip:"+userForPlatform(plt1)+"@"+server,plt1,ClientsInfo.clients(plt1))
        val p2 = ClientTestSpecParam("sip:"+userForPlatform(plt2)+"@"+server,plt2,ClientsInfo.clients(plt2))
        new CallTestSpec(system, serverActor, (p1,p2)).execute
      }
    }

    ClientsInfo.clients.keys.foreach(destroyPlatform)
  }

}
